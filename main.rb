require 'sqlite3'
require 'rack'
require 'ap'
require 'erubis'

module Developville
    class Post

        attr_reader :author, :contents, :id
        attr_writer :author, :contents
        @@counter = 0

        def initialize(author, contents)
            @author = author
            @contents = contents
            @id = @@counter
            @@counter+=1
        end
    end

    class App

        def initialize()
          @posts = []
        end

        def call(env)
            request = Rack::Request.new(env)
            response = handle_request(request)
            return response.finish()
        end

        def handle_request(request)

            Rack::Response.new do |r|
                case request.path_info
                when '/', '/posts/index'
                    r.write render('index', {posts: @posts})
                when '/posts/new'
                    r.write render('new')
                when '/posts/create'
                    if request.post?
                        @posts.push(
                            Post.new(
                                request.POST['author'],
                                request.POST['contents']
                            )
                        )
                        r.redirect '/'
                    end
                when '/posts/show'
                    post = find_post(request.GET["id"])
                    r.write render("show", {post: post})
                when '/posts/edit'
                    post = find_post(request.GET["id"])
                    r.write render("edit", {post: post})
                when '/posts/update'
                    post = find_post(request.POST["id"])
                    post.author = request.POST["author"]
                    post.contents = request.POST["contents"]
                    r.redirect '/'
                when '/posts/delete'
                    post = find_post(request.GET["id"])
                    @posts.delete(post)
                    r.redirect '/'
                else
                    r.write "404'd..."
                    r.status = 404
                end
            end
        end

        def render(name, locals={})
            path = "views/"+name+".erb"
            file = File.read(path)
            Erubis::Eruby.new(file).result(locals)
        end

        def find_post(id)
            id = id.to_i
            @posts.find{|post|post.id==id}
        end
    end
end

Rack::Handler::WEBrick.run Developville::App.new
