# Tests for Developville DB:

require 'minitest/autorun'
require '../user'

class TestUsers < MiniTest::Unit::TestCase

	def setup
		@user = Users.new
	end

	def test_a_user_exist
		assert_instance_of Class, Users
	end

	def test_user_has_id
		assert @user.id
	end

	def test_user_has_name
		assert @user.name
	end

	def test_user_has_first_name
		assert @user.first_name
	end

	def test_user_has_last_name
		assert @user.last_name
	end

	def test_user_has_birthday
		assert @user.birthday
	end

	def test_user_has_email
		assert @user.email
	end

	def test_user_email_is_valid
		assert @user.email = "/\w+[@]+\w+[.]\w{3}/"
	end

	def test_user_has_joined
		assert @user.joined
	end

end
