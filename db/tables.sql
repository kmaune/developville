-- Data tables for Developville blog:

create table users (
	id integer primary key,
	name varchar(255) not null,
	first_name varchar(255) default '',
	last_name varchar(255) default '',
	birthday varchar(10) not null,
	email varchar(255) not null,
	joined date
);

create table posts (
	id integer primary key,
	title varchar(255) not null,
	description varchar(1000),
	date_created date not null,
	posted_by integer not null,
	foreign key(posted_by) references users(id)
);

create table comments (
	id integer primary key,
	comment text default '',
	date_created date not null,
	commenter integer not null,
	foreign key(commenter) references users(id)
);
