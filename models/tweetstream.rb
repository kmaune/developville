require 'tweetstream'
require 'oauth'

# Configure my keys
TweetStream.configure do |config|
	config.consumer_key = 'your key here'
	config.consumer_secret = 'your key here'
	config.oauth_token = 'your key here'
	config.oauth_token_secret = 'your key here'
	config.auth_method = :oauth
end

TweetStream::Client.new.sample do |status|
	puts "#{status.text}"
end

# TweetStream::Client.new.track('ios8') do |status|
# 	puts "#{status.text}"
# end
